File permissions come in groups 

d --- | --- | ---

 user | group | others

You can adjust your permissions using the 'chmod' command.

* Value 4 -> read
  Value 2 -> write
  Value 1 -> execute

to change the premissions on a file or directory: 

chmod 777 File --> add read write and execute premissions to a file for user | groups | others

chmod 711 File --> read write and execute for the user and execute for groups and others.

chmod 751 File --> read write and execute for the user, group can read and execute and others can execute.

 
